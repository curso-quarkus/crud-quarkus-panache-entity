package quarkus.panache;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutoResource {

    @Inject
    private ProdutoRepository produtoRepository;

    @GET
    public List<Produto> findAll() {
        return produtoRepository.listAll();
    }

    @GET
    @Path("/page/{page}/size/{size}")
    public List<Produto> findPage(@PathParam int page, @PathParam int size) {
        PanacheQuery<Produto> produtoPanacheQuery = produtoRepository.findAll();
        return produtoPanacheQuery.page(Page.of(page, size)).list();
    }

    @POST
    @Transactional
    public Produto salvarCategoria(@Valid  Produto produto) {
        produtoRepository.persist(produto);
        return produto;

    }
}


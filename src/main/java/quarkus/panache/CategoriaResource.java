package quarkus.panache;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import io.quarkus.panache.common.Page;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/categoria")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoriaResource {

    private final CategoriaRepository categoriaRepository;

    private final CategoriaService categoriaService;

    public CategoriaResource(CategoriaRepository categoriaRepository, CategoriaService categoriaService) {
        this.categoriaRepository = categoriaRepository;
        this.categoriaService = categoriaService;
    }

    @GET
    public List<Categoria> findAll() {
        return categoriaRepository.listAll();
    }

    @GET
    @Path("/page/{page}/size/{size}")
    public List<Categoria> findPage(@PathParam int page, @PathParam int size) {
        PanacheQuery<Categoria> categoriaPanacheQuery = categoriaRepository.findAll();
        return categoriaPanacheQuery.page(Page.of(page, size)).list();
    }

    @GET
    @Path("/nome/{nome}")
    public List<Categoria> findByNome(@PathParam String nome) {
        return categoriaRepository.findByNomeLike(nome);
    }

    @POST
    @Transactional
    public Categoria salvarCategoria(Categoria categoria) {
        categoriaService.salvarCategoria(categoria);
        return categoria;

    }
}


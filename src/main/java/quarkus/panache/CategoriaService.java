package quarkus.panache;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.Valid;

@ApplicationScoped
public class CategoriaService {

    @Inject
    CategoriaRepository categoriaRepository;

    public void salvarCategoria(@Valid Categoria categoria) {
        categoriaRepository.persist(categoria);
    }

}

package quarkus.panache;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.ApplicationScoped;
import java.util.List;

@ApplicationScoped
public class CategoriaRepository implements PanacheRepository<Categoria> {

    public List<Categoria> findByNomeLike(String nome){
        return find("UPPER(nome) LIKE CONCAT('%',:nome,'%')", Parameters.with("nome", nome.toUpperCase())).list();
    }
}
